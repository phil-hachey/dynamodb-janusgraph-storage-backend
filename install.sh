#!/bin/bash

export SDKMAN_DIR=/usr/local/sdkman && curl -s https://get.sdkman.io | bash && source /usr/local/sdkman/bin/sdkman-init.sh
echo 'export SDKMAN_DIR=/usr/local/sdkman; source /usr/local/sdkman/bin/sdkman-init.sh' > /etc/profile.d/sdkman.sh
yum update && yum upgrade && yum install -y java-1.8.0-openjdk
sdk install maven < /dev/null && set -x
export GREMLIN_SERVER_USERNAME='ec2-user'
export LOG_DIR=/var/log/gremlin-server
export SERVER_DIRNAME=dynamodb-janusgraph-storage-backend-1.1.1
export SERVER_ZIP=${SERVER_DIRNAME}.zip
export PACKAGES_DIR=/usr/local/packages
export INSTALL_DIR=${PACKAGES_DIR}/${SERVER_DIRNAME}
export REPO_ARCHIVE_DIR=/dev/shm/dynamodb-janusgraph-storage-backend-master
mkdir -p ${LOG_DIR} ${INSTALL_DIR}
export SERVICE_SCRIPT=${INSTALL_DIR}/bin/gremlin-server-service.sh
pushd /dev/shm
wget https://github.com/awslabs/dynamodb-janusgraph-storage-backend/archive/master.zip && unzip -q master.zip
pushd ${REPO_ARCHIVE_DIR}
src/test/resources/install-gremlin-server.sh && popd && popd
pushd ${PACKAGES_DIR}
mv ${REPO_ARCHIVE_DIR}/server/${SERVER_DIRNAME} . && rm -rf /dev/shm/* && chmod u+x ${SERVICE_SCRIPT} && ln -s ${SERVICE_SCRIPT} /etc/init.d/gremlin-server && chkconfig --add gremlin-server
export BACKEND_PROPERTIES=${INSTALL_DIR}/conf/gremlin-server/dynamodb.properties

mkdir -p ${INSTALL_DIR}/conf/gremlin-server

cat > ${BACKEND_PROPERTIES} <<- EOM
s.backend=com.amazon.janusgraph.diskstorage.dynamodb.DynamoDBStoreManager
s.d.prefix=default
EOM

chown -R ${GREMLIN_SERVER_USERNAME}:${GREMLIN_SERVER_USERNAME} ${LOG_DIR} ${INSTALL_DIR}
ln -s ${INSTALL_DIR}/conf /home/ec2-user/conf && chmod a+r /home/ec2-user/conf
service gremlin-server start
