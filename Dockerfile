FROM amazonlinux:latest as build

ENV REPO_ARCHIVE_DIR /tmp/repo/

RUN yum update -y
RUN yum install -y util-linux which zip curl unzip findutils wget java-1.8.0-openjdk-devel

RUN wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
RUN sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
RUN yum install -y apache-maven

RUN mkdir -p ${REPO_ARCHIVE_DIR}

COPY pom.xml ${REPO_ARCHIVE_DIR}/pom.xml
COPY checkstyle.xml ${REPO_ARCHIVE_DIR}/checkstyle.xml
COPY src ${REPO_ARCHIVE_DIR}/src

ENV JAVA_HOME /usr/lib/jvm/jre-1.8.0-openjdk

WORKDIR ${REPO_ARCHIVE_DIR}
RUN ${REPO_ARCHIVE_DIR}/src/test/resources/install-gremlin-server.sh


FROM amazonlinux:latest

RUN yum update -y && yum install -y java-1.8.0-openjdk
RUN yum install -y util-linux which zip curl unzip findutils wget

ENV SERVER_DIRNAME dynamodb-janusgraph-storage-backend-1.1.1
COPY --from=build /tmp/repo/server/${SERVER_DIRNAME} /usr/local/packages

COPY run /opt/run
RUN chmod +x /opt/run

CMD ["/opt/run"]
